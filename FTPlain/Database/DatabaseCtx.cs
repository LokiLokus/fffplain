using FTPlain.Database.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FTPlain.Database {
	public class DatabaseCtx : IdentityDbContext<Login> {
		public DbSet<Login> User { get; set; }

		public DatabaseCtx(DbContextOptions<DatabaseCtx> options) : base(options)
		{
		}
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite("Data Source=test.db");

		}
	}
}