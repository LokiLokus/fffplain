﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Claims;
using FTPlain.AppSettings;
using FTPlain.Database;
using FTPlain.Database.Model;
using FTPlain.Middleware.Filter;
using FTPlain.Middleware.MiddleWare;
using LokiLogger;
using LokiLogger.LoggerAdapter;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;

namespace FTPlain {
	public class Startup {
		internal IHostingEnvironment HostingEnvironment { get; }
		internal IConfiguration Configuration { get; }
		internal static List<CultureInfo> SupportedCultures { get; private set; }

		public Startup(IConfiguration configuration, IHostingEnvironment env)
		{
			HostingEnvironment = env;
			Configuration = configuration;
			SupportedCultures = new List<CultureInfo>();
		}

		public void ConfigureServices(IServiceCollection services)
		{
			IdentitySettings identitySettings = GetSettings<IdentitySettings>("IdentitySettings");
			SwaggerSettings swaggerSettings = GetSettings<SwaggerSettings>("SwaggerSettings");
			DatabaseSettings databaseSettings = GetSettings<DatabaseSettings>("DatabaseSettings");
			LocalizationSettings localizationSettings = GetSettings<LocalizationSettings>("LocalizationSettings");
			CookieSettings cookieSettings = GetSettings<CookieSettings>("CookieSettings");
			PolicySettings policySettings = GetSettings<PolicySettings>("PolicySettings");
			
			
			foreach (string s in localizationSettings.SupportedCulture)
			{
				SupportedCultures.Add(new CultureInfo(s));
			}
			

			services.AddDbContext<DatabaseCtx>(opt =>
			{
				switch (databaseSettings.DatabaseTyp.ToLower())
				{
					case "sqlite":
						opt.UseSqlite(databaseSettings.ConnectionString);
						break;
					case "sqlserver":
						opt.UseSqlServer(databaseSettings.ConnectionString);
						break;
					case "inmemory":
						opt.UseInMemoryDatabase(databaseSettings.ConnectionString);
						break;
					default:
						throw new ArgumentException("Database Typ is not known");
				}
			});

			services.AddIdentity<Login, IdentityRole>()
				.AddEntityFrameworkStores<DatabaseCtx>()
				.AddDefaultTokenProviders();
			
			


			services.Configure<IdentityOptions>(options =>
			{
				options.Password.RequireDigit = identitySettings.Password.RequireDigit;
				options.Password.RequiredLength = identitySettings.Password.RequiredLength;
				options.Password.RequireLowercase = identitySettings.Password.RequireLowercase;
				options.Password.RequireNonAlphanumeric = identitySettings.Password.RequireNonAlphanumeric;
				options.Password.RequireUppercase = identitySettings.Password.RequireUppercase;

				options.Lockout.AllowedForNewUsers = identitySettings.Lockout.AllowedForNewUsers;
				options.Lockout.DefaultLockoutTimeSpan =
					TimeSpan.FromMinutes(identitySettings.Lockout.DefaultLockoutTimeSpanInMins);
				options.Lockout.MaxFailedAccessAttempts = identitySettings.Lockout.MaxFailedAccessAttempts;

				options.User.RequireUniqueEmail = identitySettings.User.RequireUniqueEmail;

				options.SignIn.RequireConfirmedEmail = identitySettings.SignIn.RequireConfirmedEmail;
				options.SignIn.RequireConfirmedPhoneNumber = identitySettings.SignIn.RequireConfirmedPhoneNumber;
			});


			services.AddLocalization(options =>
			{
				options.ResourcesPath = localizationSettings.ResourcesPath;
			});


			services.ConfigureApplicationCookie(options =>
			{
				options.LoginPath = cookieSettings.LoginPath;
				options.LogoutPath = cookieSettings.LogoutPath;
				options.AccessDeniedPath = cookieSettings.AccessDeniedPath;
				options.SlidingExpiration = cookieSettings.SlidingExpiration;
				options.ExpireTimeSpan = TimeSpan.FromMinutes(cookieSettings.ExpireTimeSpanInMin);
				options.Validate();
			});

			services.Configure<CookiePolicyOptions>(options =>
			{
				options.CheckConsentNeeded = context => true;
				options.MinimumSameSitePolicy = SameSiteMode.None;
			});


			services.Configure<RequestLocalizationOptions>(options =>
			{
				options.DefaultRequestCulture = new RequestCulture(localizationSettings.DefaultCulture);
				options.SupportedCultures = SupportedCultures;
				options.SupportedUICultures = SupportedCultures;

				options.RequestCultureProviders.Add(new QueryStringRequestCultureProvider());
				options.RequestCultureProviders.Add(new CookieRequestCultureProvider());
				options.RequestCultureProviders.Add(new AcceptLanguageHeaderRequestCultureProvider());
			});

			services.ConfigureSwaggerGen(options => { options.CustomSchemaIds(x => x.FullName); });

			services.AddSwaggerGen(c => { c.SwaggerDoc(swaggerSettings.Name, new Info()); });

			services.AddMvc(options => { options.Filters.Add(new LanguageSupportFilter()); })
				.AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
				.AddDataAnnotationsLocalization()
				.AddJsonOptions(options =>
				{
					options.SerializerSettings.ContractResolver = new DefaultContractResolver();
					options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
				})
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);


			services.AddAuthorization(
				options =>
				{
					foreach (KeyValuePair<string, List<string>> policy in policySettings.Policies)
					{
						options.AddPolicy(policy.Key, polOpt =>
						{
							policy.Value.ForEach(x => { polOpt.RequireClaim(x); });
						});						
					}
				});


			Loki.ProjectNameSpace = "FTPlain";
			Loki.UpdateAdapter(new BasicLoggerAdapter());
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			SwaggerSettings swaggerSettings = GetSettings<SwaggerSettings>("SwaggerSettings");
			
			if (env.IsDevelopment())
				app.UseDeveloperExceptionPage();
			else
				app.UseHsts();

			

			IOptions<RequestLocalizationOptions> locOptions =
				app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();

			app.UseAuthentication();
			
			app.UseRequestLocalization(locOptions.Value);

			app.UseMiddleware<LoggingMiddleware>();

			app.UseStaticFiles();
			app.UseCookiePolicy();


			app.UseSwagger();
			app.UseSwaggerUI(c => { c.SwaggerEndpoint(swaggerSettings.EndPoint, swaggerSettings.Name); });


			app.UseMvc(routes =>
			{
				routes.MapRoute(
					"default",
					"{controller=Login}/{action=Login}/{id?}");
			});
		}
		
		
		/// <summary>
		/// Obtains Section Data
		/// </summary>
		/// <param name="section"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		/// <exception cref="NullReferenceException"></exception>
		private T GetSettings<T>(string section)
		{
			T setting = Configuration.GetSection(section).Get<T>();
			if (setting == null) throw new NullReferenceException(section + " is null");
			return setting;
		}
	}
}