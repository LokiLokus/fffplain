using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FTPlain.Database;
using FTPlain.Database.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Localization;

namespace FTPlain.Controller {
	[Route("Test")]
	public class TestController  : ControllerAbstract.ControllerAbstract{
		public TestController(UserManager<Login> userManager, SignInManager<Login> signInManager, DatabaseCtx context, IHtmlLocalizer<SharedResources> localizer) : base(userManager, signInManager, context, localizer)
		{
		}

		[HttpGet]
		public async Task<IActionResult> Index()
		{
			var admin = await _userManager.FindByNameAsync("Admin");
			if (admin == null)
			{
				var res = await _userManager.CreateAsync(new Login()
				{
					UserName = "Admin",
					Email = "sailerq@in.tum.de"
				}, "1234");
				if(res.Succeeded) Console.WriteLine("NICE");
			}
			
			admin = await _userManager.FindByNameAsync("Admin");
			await _userManager.AddClaimAsync(admin, new Claim("joo","joo"));
			return Ok("Hallo");
		}

		[HttpGet("Login")]
		public async Task<IActionResult> Login()
		{
			var admin = await _userManager.FindByNameAsync("Admin");
			
			return Ok(await _signInManager.PasswordSignInAsync(admin,"1234",false,true));
		}

		[HttpGet("Claims")]
		public async Task<IActionResult> Claims()
		{
			
			var admin = await _userManager.FindByNameAsync(User.Identity.Name);
			return Ok(User.Claims);
		}

		[HttpGet("Dudes")]
		public IActionResult Dudes()
		{
			return Ok("JOOO");
		}
		
		[HttpGet("Emploee")]
		public IActionResult DerDa()
		{
			return Ok("JOOO");
		}

		[HttpGet("DateTime")]
		public IActionResult DateTimeShow()
		{
			return Ok(DateTime.Now);
		}
	}
}