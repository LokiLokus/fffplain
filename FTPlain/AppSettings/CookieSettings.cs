namespace FTPlain.AppSettings {
	public class CookieSettings {
		public string LoginPath { get; set; }
		public string LogoutPath { get; set; }
		public string AccessDeniedPath { get; set; }
		public bool SlidingExpiration { get; set; }
		public int ExpireTimeSpanInMin { get; set; }
	}
}