using System.Collections.Generic;

namespace FTPlain.AppSettings {
	public class LocalizationSettings {
		public string ResourcesPath { get; set; }
		public IList<string> SupportedCulture { get; set; }
		public string DefaultCulture { get; set; }
	}
}