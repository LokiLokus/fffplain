using System.Collections.Generic;

namespace FTPlain.AppSettings {
	public class PolicySettings {
		public Dictionary<string,List<string>> Policies { get; set; }
	}
}