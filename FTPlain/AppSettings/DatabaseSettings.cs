namespace FTPlain.AppSettings {
	public class DatabaseSettings {
		public string ConnectionString { get; set; }
		public string DatabaseTyp { get; set; }
	}
}