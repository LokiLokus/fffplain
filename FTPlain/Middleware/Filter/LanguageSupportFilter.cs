using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FTPlain.Middleware.Filter {
	public class LanguageSupportFilter : ActionFilterAttribute {
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			IRequestCultureFeature rqf = context.HttpContext.Features.Get<IRequestCultureFeature>();
			CultureInfo culture = rqf.RequestCulture.Culture;
			string languageTag = culture.IetfLanguageTag;
			Microsoft.AspNetCore.Mvc.Controller viewData = context.Controller as Microsoft.AspNetCore.Mvc.Controller;
			if (viewData?.ViewData != null) viewData.ViewData["Language"] = languageTag;

			base.OnActionExecuting(context);
		}
	}
}