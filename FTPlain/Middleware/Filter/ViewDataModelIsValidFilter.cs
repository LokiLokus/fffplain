using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FTPlain.Middleware.Filter {
	public class ViewDataModelIsValidFilter : ActionFilterAttribute {
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			if (!context.ModelState.IsValid)
			{
				Microsoft.AspNetCore.Mvc.Controller
					viewData = context.Controller as Microsoft.AspNetCore.Mvc.Controller;
				if (viewData?.ViewData != null)
					viewData.ViewData["error"] = context.ModelState.Values
						.SelectMany(e => e.Errors)
						.Select(e => e.ErrorMessage);
				context.Result = new ViewResult();
			}
			else
			{
				base.OnActionExecuting(context);
			}
		}
	}
}