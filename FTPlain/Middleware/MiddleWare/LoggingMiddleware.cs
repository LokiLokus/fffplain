using System.Globalization;
using System.Threading.Tasks;
using LokiLogger;
using LokiLogger.Model;
using Microsoft.AspNetCore.Http;

namespace FTPlain.Middleware.MiddleWare {
	public class LoggingMiddleware {
		private readonly RequestDelegate _next;

		public LoggingMiddleware(RequestDelegate next)
		{
			_next = next;
		}

		public async Task InvokeAsync(HttpContext context)
		{
			Loki.Information("New HttpRequest");
			await _next(context);
		}
	}
}