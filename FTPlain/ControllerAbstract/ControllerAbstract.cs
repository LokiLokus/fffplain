using FTPlain.Database;
using FTPlain.Database.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Localization;

namespace FTPlain.ControllerAbstract {
	[ApiController]
	public abstract class ControllerAbstract : Microsoft.AspNetCore.Mvc.Controller{
		protected readonly DatabaseCtx _dbContext;
		protected readonly IHtmlLocalizer<SharedResources> _localizer;
		protected readonly SignInManager<Login> _signInManager;
		protected readonly UserManager<Login> _userManager;
		
		public ControllerAbstract(UserManager<Login> userManager, SignInManager<Login> signInManager, DatabaseCtx context,
			IHtmlLocalizer<SharedResources> localizer)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_dbContext = context;
			_localizer = localizer;
		}
	}
}